// api url
const api_url =
	"https://citcall.com/test/countries.json";

// Defining async function
async function getapi(url) {
	
	// Storing response
	const response = await fetch(url);
	
	// Storing data in form of JSON
	var data = await response.json();
	console.log(data);
	if (response) {
		hideloader();
	}
	showname(data);
	showdial(data);
	showflag(data);
	showiso(data)
}
// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
	document.getElementById('loading').style.display = 'none';
}
// Function to define innerHTML for HTML table
function showname(data) {
	let tab =
		`<tr>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
	<td>${r.name} </td>
		
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("name").innerHTML = tab;
}

function showdial(data) {
	let tab =
		`<tr>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
		<td>${r.dialCode} </td>
		
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("dial").innerHTML = tab;
}

function showiso(data) {
	let tab =
		`<tr>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
	<td>${r.isoCode} </td>
		
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("iso").innerHTML = tab;
}

function showflag(data) {
	let tab =
		`<tr>
		</tr>`;
	
	// Loop to access all rows
	for (let r of data) {
		tab += `<tr>
	<td src={${r.flag}}>${r.flag} </td>
		
</tr>`;
	}
	// Setting innerHTML as tab variable
	document.getElementById("flag").innerHTML = tab;
}
